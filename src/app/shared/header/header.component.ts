import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BackendService } from '../../services/backend-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent implements OnInit {
  @Input() imageUrl: string;
  @Input() pageTitle: string;
  @Input() helpType: string;
  emailSent = false;
  selectedValue;
  formShowing = false;
  configData;

  error: any;
  dataLoading: boolean = false;
  brokenNetwork = false;

  langs: string[] = [];

  constructor(private _backendService: BackendService, private translate: TranslateService){
    this.translate.setDefaultLang('es');
    this.translate.use('es');
    this.translate.addLangs(['es','en']);
    this.langs = this.translate.getLangs();
  }

  changeLang(lang: string){
    this.translate.use(lang);
  }

  ngOnInit(){
    this.configData = this._backendService.getConfig();
  }

  onSubmit(formData) {
    this.dataLoading = true;
    //console.log(formData);
    this._backendService.sendEmail(formData).subscribe(
      res => {
        //console.log(res);
      },
      error => {
        //console.log(error);
        console.log("API didn't respond.");
        this.brokenNetwork = true;
        this.dataLoading = false;
      },
      () => {
        this.dataLoading = false;
        this.emailSent = true;
      }
    )
  }
}